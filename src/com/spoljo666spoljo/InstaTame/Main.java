package com.spoljo666spoljo.InstaTame;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

	public static boolean update = false;
	public static String name = "";
	public static long size = 0;

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
		try {
			final MetricsLite metrics = new MetricsLite(this);
			metrics.start();
		} catch (final IOException e) {
			Bukkit.getLogger().warning("[" + getDescription().getName() + "] PluginMetrics > Failed to submit the stats :-(");
		}
		final File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveDefaultConfig();
			try {
				final FileWriter fw = new FileWriter(f);
				fw.write("# " + getDescription().getName() + " v" + getDescription().getVersion() + " by spoljo666spoljo" + "\n" + "\n");

				fw.write("# If true, Update-Checker will be enabled and will download the newest version of " + getDescription().getName() + " whenever there is one" + "\n");
				fw.write("update-checker: true"/* + "\n" */);

				fw.flush();
				fw.close();
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
			if (getConfig().getBoolean("update-checker") == true) {
				final Updater updater = new Updater(this, "instatame", getFile(), Updater.UpdateType.DEFAULT, true);
				update = updater.getResult() == Updater.UpdateResult.UPDATE_AVAILABLE;
				name = updater.getLatestVersionString();
				size = updater.getFileSize();
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onHorseSpawnEvent(final PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		if (p.hasPermission("instatame.tameegg")) {
			final Block b = e.getClickedBlock();
			final Action a = e.getAction();
			if (a.equals(Action.RIGHT_CLICK_BLOCK) && p.getItemInHand().getType() == Material.MONSTER_EGG && p.getItemInHand().getDurability() == 100) {
				e.setCancelled(true);
				final Horse horse = (Horse) p.getWorld().spawnEntity(b.getLocation(), EntityType.HORSE);
				horse.setTamed(true);
				horse.setOwner(p);
				horse.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));
				horse.setPassenger(p);
			}
		}
	}

	@EventHandler
	public void onPlayerInteractHorse(final PlayerInteractEntityEvent e) {
		if (e.getRightClicked() instanceof Horse) {
			final Player p = e.getPlayer();
			final Horse horse = (Horse) e.getRightClicked();
			if (p.hasPermission("instatame.tamewild")) {
				if (!horse.isTamed()) {
					e.setCancelled(true);
					horse.setTamed(true);
					horse.setOwner(p);
					horse.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));
					horse.setPassenger(p);
				}
			}
		}
	}
}
